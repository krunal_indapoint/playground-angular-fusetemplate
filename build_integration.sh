#!/bin/bash

source ~/.bashrc
docker pull rewardle/deployer:angular8

echo "AQICAHhsH2bUVFO23zfKiXraXy+9Rcg9LHDX35kgL21t4cQrEgELBslddAIcZQQAkdvNCywhAAAAgzCBgAYJKoZIhvcNAQcGoHMwcQIBADBsBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDAONm9EYEYt0St9aLAIBEIA/XZyrS9wBQPzJnv250lBi233kIym0x4cN40rwwn+h5YoQk4rX5Pgxb2azInnJsHhCBsAQ/8lwBCFoZ1jQumfm" > ./tempfile

cat ./tempfile | base64 -d > ./base64encrypted

TOKEN=$(aws kms decrypt --region ap-southeast-2 --ciphertext-blob fileb://base64encrypted --query Plaintext --output text | base64 --decode)
printf "//registry.npmjs.org/:_authToken=$TOKEN" > ~/.npmrc

echo "Node Version"
node --version
echo "npm version"
npm --version
echo "Angular Cli version"
ng version

echo "Restore packages"
npm install

echo "Build and prepare package"
ng build --env=integration
STATUS=$?
if [ $STATUS -eq 0 ]
then
  zip -r dist.zip dist
  echo "Upload package to s3"
  docker run -it \
    -v `pwd`:/app \
    --entrypoint=aws rewardle/deployer:angular8 \
    s3 cp --region=ap-southeast-2 --acl public-read \
    /app/dist.zip s3://rewardleintegrationbuild/builds/$BUILDKITE_PIPELINE_SLUG/$BUILDKITE_BUILD_NUMBER/
fi

echo "Clean up"
rm -rf dist
rm -f dist.zip
rm -rf node_modules
rm -f ./base64encrypted
rm -f ./tempfile
rm -f ~/.npmrc
exit $STATUS
